all: mac_ip

%: %.ml
	ocamlfind ocamlopt -w x -dtypes -predicates opt -thread -package str,syslog,postgresql -linkpkg -o $@ $<

clean:
	rm -f *.cm* *.o *.native *.annot mac_ip

install: mac_ip
	cp mac_ip /usr/local/bin/mac_ip
	cp mac_ip.init /etc/init.d/mac_ip
