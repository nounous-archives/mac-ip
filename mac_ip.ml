(** Command line handling *)
module Clflags = struct
  let daemonize = ref false
  let interface = ref "crans"
  let debug = ref 3
  let pidfile = ref None
  let syslog = ref false
  let hostname=ref (Unix.gethostname())
  let file= ref None
  let date_reg = ref "\\([^-]+\\)-\\([^-]+\\)-\\([^T]+\\)T\\([^:]+\\):\\([^:]+\\):\\([^\\.]+\\).*"
  let date_format = ref (Some [ ("year", "int", 1); ("month", "int", 2); ("day", "int", 3); ("hour", "int", 4); ("min", "int", 5); ("sec", "int", 6) ])

  let process = ref "mac_ip"

  let cmdline_spec = [
    "-b", Arg.Set daemonize, " Go to background";
    "-I", Arg.Set_string interface, Printf.sprintf "<interface>  Capturing interface (default: %s)" !interface;
    "-d", Arg.Set_int debug, Printf.sprintf "<n>  Debugging level (default: %d)" !debug;
    "-p", Arg.String (fun x -> pidfile := Some x), "<pidfile>  Write master PID to file (default: none)";
    "-f", Arg.String (fun x -> file:=Some x), "<file> Logfile to parse";
    "--force-syslog", Arg.Set syslog, " Force syslog even when running in foreground mode";
  ]

  let anonfun s =
    raise (Arg.Bad (Printf.sprintf "do not know what to do with %s" s))

  let usage_msg =
    Printf.sprintf "%s [options]" Sys.argv.(0)

  let () =
    Arg.parse cmdline_spec anonfun usage_msg;
    if !daemonize then syslog := true
end

let date_reg_default = Str.regexp "\\([^ ]+\\) +\\([0-9]+\\) +\\([0-9]+\\):\\([0-9]+\\):\\([0-9]+\\).*"
and date_format_default =  [ ("month", "str", 1); ("day", "int", 2); ("hour", "int", 3); ("min", "int", 4); ("sec", "int", 5) ]


let reg = Str.regexp (Printf.sprintf 
  ".* %s .* IN=%s .* MAC=\\([^ ]+\\) SRC=\\([^ ]+\\) DST=\\([^ ]+\\) .*"
  !Clflags.hostname
  !Clflags.interface)
and date_reg = Str.regexp !Clflags.date_reg
and newline_re = Str.regexp "[ \t]*\n[ \t]*"
(*and mac_komaz="00:19:bb:31:3b:80"*)
and mac_pioneer="d4:ae:52:92:f0:9f"
and prefix_ip = "2a01:240:fe3d:4:" 
let assoc_v4=Hashtbl.create 10
and assoc_v6=Hashtbl.create 10  

let level_of_int : int -> Syslog.level = function
  | 0 -> `LOG_ERR
  | 1 -> `LOG_WARNING
  | 2 -> `LOG_NOTICE
  | 3 -> `LOG_INFO
  | 4 -> `LOG_DEBUG
  | _ -> raise Exit 
let debug level fmt =
  Printf.ksprintf begin fun msg ->
    if level <= !Clflags.debug then begin
      if !Clflags.syslog then begin
        (* syslog-ng seems to flush logs only once per
           openlog/closelog, so we call them each time *)
        try
          let level = level_of_int level in
          let h = Syslog.openlog ~facility:`LOG_DAEMON !Clflags.process in
          Syslog.syslog h level msg;
          Syslog.closelog h
        with Exit -> (* level not logged with syslog *)
          ()
      end;
      Printf.printf "%d: " level;
      print_endline msg
    end else ()
  end fmt 
  
let eui64 mac=
  try
    let reg = Str.regexp ":" in
    let mac=Array.map 
      (fun i -> Int64.of_string (Printf.sprintf "0x%s" i)) 
      (Array.of_list (Str.split reg mac)) in
    mac.(0)<-Int64.logxor mac.(0) 2L;
    Printf.sprintf "%s%02Lx%02Lx:%02Lxff:fe%02Lx:%02Lx%02Lx" prefix_ip
    mac.(0) mac.(1) mac.(2) mac.(3) mac.(4) mac.(5);
  with 
    Failure "int_of_string" -> ""
    |  Invalid_argument "index out of bounds" -> "" 
  

let connection= ref None
let rec connect ?conn:(conn=false) () =
  if not conn then 
    match !connection with 
	| Some pq -> pq
	| None -> let pq = new Postgresql.connection ~host:"pgsql.adm.crans.org" ~user:"crans" ~dbname:"filtrage" () in
	          connection:= Some (pq); pq
  else begin
    (try 
      (match !connection with
	| Some pq -> pq#finish;
	| None -> ());
    with Postgresql.Error e ->
    debug 0 "E: PosgreSQL error: %s" (Str.global_replace newline_re " " (Postgresql.string_of_error e))
    );
    connection:= Some (new Postgresql.connection ~host:"pgsql.adm.crans.org" ~user:"crans" ~dbname:"filtrage" ());
    connect();
  end
	
let rec insert_to_pg date mac ip = 
try
    let pq = connect() in	         
    let do_insert = Printf.ksprintf
      (fun query ->
         let expect = [Postgresql.Command_ok] in
         debug 10 "executing SQL query: %s" query;
         ignore (pq#exec ~expect query))
      (* ugly, but we want lenny compatibility! *)
      "INSERT INTO mac_ip (date, ip, mac) VALUES ('%s', '%s', '%s');"
    in
    do_insert date ip (eui64 mac);
    (*pq#finish;*)
  with Postgresql.Error e ->
    debug 0 "E: PosgreSQL error: %s" (Str.global_replace newline_re " " (Postgresql.string_of_error e));
    match e with
	| Postgresql.Connection_failure(str) -> ignore(connect ~conn:true ()); insert_to_pg date mac ip;
	| _ -> ()


let rec delete_old_record() = 
try
  let pq = connect() in
  let expect = [Postgresql.Command_ok] in
  let _=pq#exec ~expect "DELETE FROM mac_ip WHERE date < current_date - interval '5 day'" in
  (*pq#finish*)
  ()
with Postgresql.Error e  ->
    debug 0 "E: PosgreSQL error: %s" (Str.global_replace newline_re " " (Postgresql.string_of_error e));
    (match e with
  | Postgresql.Connection_failure(str) -> ignore(connect ~conn:true ()); delete_old_record();
  | _ -> ())

let rec get_last_timestamp()=
try
  let pq = connect() in
  let expect = [Postgresql.Tuples_ok] in
  let data=pq#exec ~expect "SELECT date FROM mac_ip ORDER BY date desc LIMIT 1;" in
    (*pq#finish*)
    data#get_all.(0).(0)
with Postgresql.Error e  ->
    debug 0 "E: PosgreSQL error: %s" (Str.global_replace newline_re " " (Postgresql.string_of_error e));
    (match e with
	| Postgresql.Connection_failure(str) -> ignore(connect ~conn:true ()); get_last_timestamp();
	| _ -> "0000-00-00 00:00:00")

    |  Invalid_argument("index out of bounds") -> "0000-00-00 00:00:00"

      
    
let last_time = get_last_timestamp()
let _ = delete_old_record()
let add_mac_ip assoc ip mac date=  
  if not (Hashtbl.mem assoc ip) then (
        debug 4 "create %s for %s at %s" mac ip date;
        Hashtbl.replace assoc ip mac ;
        if date >= last_time then
          insert_to_pg date mac ip;
        true;
  )
  else if Hashtbl.find assoc ip <> mac then (
      debug 4 "replace %s by %s for %s at %s" (Hashtbl.find assoc_v4 ip) mac ip date;
      Hashtbl.replace assoc ip mac;
      if date >= last_time then
        insert_to_pg date mac ip;
      true;
    ) else
    false 
    
let ipv6 ip mac date=  add_mac_ip assoc_v6 ip mac date
and ipv4 ip mac date=  add_mac_ip assoc_v4 ip mac date

let month_of_string str = match str with
  | "Jan" ->1
  | "Feb" ->2
  | "Mar" ->3
  | "Apr" ->4
  | "May" ->5
  | "Jun" ->6
  | "Jul" ->7
  | "Aug" ->8
  | "Sep" ->9
  | "Oct" ->10
  | "Nov" ->11
  | "Dec" ->12
  | s -> debug 0 "Unkown month %s" s; failwith s


let extract_date str =
    let date=Array.make 6 (-1) in
    let rec aux str l = match l with
      | [] -> ();
      | (m, _, pos)::q when m = "year" -> date.(0) <- int_of_string(Str.matched_group pos str); aux str q;
      | (m, typ, pos)::q when m = "month" -> date.(1) <- if typ = "int" then int_of_string(Str.matched_group pos str) else month_of_string(Str.matched_group pos str); aux str q;
      | (m, typ, pos)::q when m = "day" -> date.(2) <- int_of_string(Str.matched_group pos str); aux str q;
      | (m, typ, pos)::q when m = "hour" -> date.(3) <- int_of_string(Str.matched_group pos str); aux str q;
      | (m, typ, pos)::q when m = "min" -> date.(4) <- int_of_string(Str.matched_group pos str); aux str q;
      | (m, typ, pos)::q when m = "sec" -> date.(5) <- int_of_string(Str.matched_group pos str); aux str q
      | (_, _, _)::q -> debug 0 "Date_reg or date_format param are incorrect"; failwith("Date_reg or date_format param are incorrect")
    in
    let fill_array str date_format =
        aux str date_format;
        if date.(0) < 0 then
          date.(0) <-if date.(1) > (Unix.gmtime (Unix.time())).Unix.tm_mon + 1 then
            1900+(Unix.gmtime (Unix.time())).Unix.tm_year - 1
          else 1900+(Unix.gmtime (Unix.time())).Unix.tm_year
    in
    (match !Clflags.date_format with
      | Some(format) ->
    if Str.string_match date_reg str 0 then
        fill_array str format
    else if Str.string_match date_reg_default str 0 then
        fill_array str date_format_default
    else begin
        debug 0 "Unable to extract date"; failwith("Unable to extract date")
    end;
      | None ->
    if Str.string_match date_reg_default str 0 then
        fill_array str date_format_default
    else begin
        debug 0 "Unable to extract date"; failwith("Unable to extract date")
    end);

    Printf.sprintf "%04d-%02d-%02d %02d:%02d:%02d" date.(0) date.(1) date.(2) date.(3) date.(4) date.(5)

let rec open_file ?wait:(wait=1) () = match !Clflags.file with
    | Some file -> (
        if Sys.file_exists file then
          Unix.openfile file [Unix.O_RDONLY] 0o640
        else begin
          debug 0 "File %s not found, retry later…" file;
          Unix.sleep wait;
          open_file ~wait:(min (wait*2) 120) ();
        end
      )
    | None -> debug 0 "No file to parse"; exit(1); Unix.stdin


let main() =
  let file = open_file() in
  let tmp = String.create (256) in
  let cache=ref "" in
  let readline socket =
    let line=Buffer.create 256 in
    let rec _readall () =
      let count = (Unix.read socket tmp 0 256 ) in
      let tmp=String.sub tmp 0 count in
      if count = 0 then begin
        let _ = Unix.select [] [] [] 0.1 in
        let tmp=Buffer.contents line in 
        Buffer.clear line;
        tmp;
      end else
        try 
          (*print_string tmp;*)
          Buffer.add_string line !cache;
          let n = String.index tmp '\n' in
          cache:=(String.sub tmp (n+1) (String.length tmp - n -1));
          flush_all();
          if n>1 then 
            Buffer.add_substring line tmp 0 (n-1);
          let tmp=Buffer.contents line in 
          Buffer.clear line;
          tmp;
       with Not_found ->
            Buffer.add_string line tmp;
          _readall ();
  in
          try 
            let n = String.index !cache '\n' in
            if n>1 then 
              Buffer.add_substring line !cache 0 (n-1); 
            cache:=(String.sub !cache (n+1) (String.length !cache - n - 1));
            let tmp=Buffer.contents line in 
            Buffer.clear line;
            tmp;
          with Not_found ->
            _readall() in

    let write_pidfile = match !Clflags.pidfile with
    | Some x ->
        (* we open the pid file prior to going to background, in case of error *)
        let pidfile = open_out x in
        lazy begin
          let pid = Unix.getpid () in
          Printf.fprintf pidfile "%d\n%!" pid;
          close_out pidfile;
          pid
        end
    | None ->
        Lazy.lazy_from_fun Unix.getpid
  in
  if !Clflags.daemonize then begin
    (* redirect standard channels *)
    let devnull = Unix.openfile "/dev/null" [Unix.O_RDWR] 0o644 in
    Unix.dup2 devnull Unix.stdout;
    Unix.dup2 devnull Unix.stderr;
    Unix.dup2 devnull Unix.stdin;
    Unix.close devnull;
    (* double-fork magic *)
    if Unix.fork ()  > 0 then exit 0;
    Sys.chdir "/";
    ignore (Unix.setsid ());
    ignore (Unix.umask 0);
    if Unix.fork () > 0 then exit 0;
  end;

  (* but we write the pid after going to background! *)
  let master = Lazy.force write_pidfile in
  Clflags.process := Printf.sprintf "mac_ip_%s[%d]_%s" !Clflags.interface master (match !Clflags.file with Some x -> x | None -> "");
  while true do
    let str = readline file in
    (*Printf.printf "%s\n" str;*)
    try
    if Str.string_match reg str 0 then begin
      let mac=Str.matched_group 1 str
      and ip_src=Str.matched_group 2 str
      (*and ip_dst=Str.matched_group 3 str*)
      in 
      let timestamp = extract_date str in
      if String.length mac = 41 then
        let mac_src=String.sub mac 18 17 
        (*and mac_dst=String.sub mac 0 17*)
        and eth_type = String.sub mac 36 5 in
        if mac_src <> mac_pioneer then
          (match eth_type with
            | "86:dd" -> ignore (ipv6 ip_src mac_src timestamp);flush_all(); (*ipv6*)
            | "08:00" -> () (*ipv4*)
            | _ -> ()
          )
    end
    with 
      | Invalid_argument "String.sub" -> Printf.fprintf stderr "%s" (str^"\n");
      | Not_found -> debug 0 "Not_found: %s" str;
    
   
   
   done;;

main();;
